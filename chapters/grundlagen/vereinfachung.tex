\section{Vereinfachung von GPS-Routen}
\label{sec:grundlagen-vereinfachung}

Die Daten der GPS-Routen sind wie unter Punkt~\vref{subsec:grundlagen-strava-aufloesung} in einer hohen Auflösung verfügbar. Dies bietet zum einen die Möglichkeit von sehr präzisen Ergebnissen der Algorithmen, bringt aber auch den Nachteil, dass eine entsprechende Berechnung mehr Ressourcen und Zeit in Anspruch nimmt.

In Abschnitt~\vref{sec:evaluierung-performance} wird die Performance, abhängig von der Auflösung der Datenreihe, evaluiert. Demzufolge ist es notwendig die Anzahl der gegebenen Daten zu reduzieren. Wichtig ist zu beachten, dass eine Reduktion in der Anzahl der verfügbaren Daten auch einen Verlust der Datenqualität zur Folge hat, da die Auflösung reduziert wird.

Eine naive Herangehensweise wäre beispielsweise das einfache Entfernen jedes zweiten Datenpunktes. Bei Betrachtung von Abbildung~\vref{fig:grundlagen-vereinfachung-naiv} wird allerdings ersichtlich, dass dies bei der für die Richtung der Route relevanten Koordinaten die Genauigkeit der Route stark beeinflussen kann.

\begin{figure}
	\begin{subfigure}{.5\textwidth}
		\includestandalone{images/grundlagen/vereinfachung/naiv-1}
		\subcaption{Ursprungsroute vor dem Entfernen von Punkten.}
	\end{subfigure}
	\begin{subfigure}{.5\textwidth}
		\includestandalone{images/grundlagen/vereinfachung/naiv-2}
		\subcaption{Routenänderung nach dem naiven Entfernen eines Punktes.}
	\end{subfigure}

	\caption{Problem bei einer naiven Herangehensweise eine Route zu vereinfachen.}
	\label{fig:grundlagen-vereinfachung-naiv}
\end{figure}

Demnach wird für die Vereinfachung der GPS-Routen ein zusätzlicher Algorithmus benötigt. Dieser entfernt lediglich GPS-Koordinaten, die zu einer geringeren Richtungsänderung der Route führen. Somit wird die Route nur geglättet und nicht signifikant verändert (siehe Abbildung~\vref{fig:grundlagen-vereinfachung-simplify-js}).

\begin{figure}
	\begin{subfigure}{.5\textwidth}
		\includegraphics[width=\linewidth]{grundlagen/vereinfachung/simplify-js-before}
		\subcaption{GPS-Route vor der Vereinfachung.}
	\end{subfigure}
	\begin{subfigure}{.5\textwidth}
		\includegraphics[width=\linewidth]{grundlagen/vereinfachung/simplify-js-after}
		\subcaption{GPS-Route nach der Vereinfachung.}
	\end{subfigure}

	\caption{Die Vereinfachung einer GPS-Route durch Simplify.js.~\cite{simplify-js}}
	\label{fig:grundlagen-vereinfachung-simplify-js}
\end{figure}

Der Ramer-Douglas-Peucker-Algorithmus ist ein simpler Algorithmus, der eben dieses Verhalten hat und nur Punkte entfernt, die keine signifikante Richtungsänderung der Route bedeuten. Der Algorithmus kann allgemein auf alle Kurven sowie in der Bildverarbeitung angewendet werden und dient somit beispielsweise auch der Simplifizierung von Kurvendiagrammen oder Vektorgrafiken.~\cite{ramer1972iterative, douglas1973algorithms}

Nach der schrittweisen Erläuterung des Algorithmus in~\cite[536]{bolc2012computer} lässt sich daraus Listing~\vref{lst:grundlagen-vereinfachung} implementieren. Neben dem zu vereinfachenden Pfad wird hier zudem ein $\epsilon$ als Parameter angegeben.
Dieser Parameter gibt die Distanz an, welche eine Koordinate $P$ maximal von dem Segment $ S $ entfernt sein darf. $S$~entspricht dem Segment, welches bei jeder Iteration die Anfangs- und Endkoordinate direkt verbindet. Punkte mit einer Abweichung kleiner $\epsilon$ werden aus der Route entfernt.

In Abbildung~\vref{fig:grundlagen-vereinfachung-algorithmus} ist schrittweise eine Vereinfachung einer simplen Kurve dargestellt. In den Abbildungen~\vref{fig:grundlagen-vereinfachung-algorithmus-2} und~\vref{fig:grundlagen-vereinfachung-algorithmus-3} wird dargestellt, wie weit die Punkte zwischen der Linie der aktuellen Anfangs- und Endpunkte entfernt sind.

\begin{lstlisting}[float, language={JavaScript}, caption={Ramer-Douglas-Peucker-Algorithmus}, label={lst:grundlagen-vereinfachung}]
function simplify(path, epsilon) {
  if (path.length < 2) {
    return [ path[0] ];
  }

  let maxDistance = 0;
  let maxDistanceIndex = -1;

  const end = path.length - 1;
  for (let i = 1; i < end; ++i) {
    const distance = distanceToLine(path[i], [ path[0], path[end] ]);

    if (distance > dmax) {
      maxDistance = d;
      maxDistanceIndex = i;
    }
  }

  if (maxDistance > epsilon) {
    const recResults1 = simplifyRec(path.slice(0, maxDistanceIndex + 1), epsilon);
    const recResults2 = simplifyRec(path.slice(maxDistanceIndex), epsilon);
    return [
      ...recResults1.slice(0, -1),
      ...recResults2,
    ];
  } else {
    return [ path[0], path[end] ];
  }
}
\end{lstlisting}

\begin{figure}
	\begin{subfigure}{.5\linewidth}
		\includestandalone[width=\linewidth]{images/grundlagen/vereinfachung/algorithmus-1}
		\caption{Die Originalroute.}
		\label{fig:grundlagen-vereinfachung-algorithmus-1}
	\end{subfigure}
	\begin{subfigure}{.5\linewidth}
		\includestandalone[width=\linewidth]{images/grundlagen/vereinfachung/algorithmus-2}
		\caption{Suchen des am weitesten entfernten Punktes $P_3$ unter Verwendung von $\epsilon$.}
		\label{fig:grundlagen-vereinfachung-algorithmus-2}
	\end{subfigure}
	\begin{subfigure}{.5\linewidth}
		\includestandalone[width=\linewidth]{images/grundlagen/vereinfachung/algorithmus-3}
		\caption{Suchen der am weitesten entfernten Punkte $P_2$ und $P_4$ unter Verwendung von $\epsilon$.}
		\label{fig:grundlagen-vereinfachung-algorithmus-3}
	\end{subfigure}
	\begin{subfigure}{.5\linewidth}
		\includestandalone[width=\linewidth]{images/grundlagen/vereinfachung/algorithmus-4}
		\caption{Die vereinfachte Route nach Entfernung des Punktes $P_4$.}
		\label{fig:grundlagen-vereinfachung-algorithmus-4}
	\end{subfigure}

	\caption{Schritte des Ramer–Douglas–Peucker-Algorithmus zur Vereinfachung einer GPS-Route.}
	\label{fig:grundlagen-vereinfachung-algorithmus}
\end{figure}

In Abbildung~\vref{fig:grundlagen-vereinfachung-epsilon} wird beispielhaft anhand einer~\SI{42}{\kilo\meter} und~\SI{2}{\hour} langen Aktivität\footnote{\url{https://www.strava.com/activities/1477655699}} dargestellt, wie sich die Anzahl der Datenpunkte bei unterschiedlichen $\epsilon$ verändert. $\epsilon = 0$ entspricht hierbei der originalen Datenreihe ohne entfernte Datenpunkte. Zudem wird die Veränderung der Gesamtdistanz verglichen.

Die Berechnungen dauern im erstellten Prototypen\footnote{\url{https://activity-matcher.andipaetzold.com/simplify?activity=1477655699}} \SIrange{10}{100}{\milli\second} und weisen keine Tendenz bei der Veränderung von $\epsilon$ auf.
Es wird deutlich, dass sich die Anzahl der Datenpunkte bereits bei einem geringen $\epsilon$ massiv verringert, dabei allerdings die Gesamtdistanz kaum beeinflusst. Diese verringert sich erst bei sehr hohen Werten für $\epsilon$.

\begin{table}
	\centering	
	\resizebox{0.7\textwidth}{!}{
		\includestandalone{images/grundlagen/vereinfachung/chart}
	}

	\caption{Anzahl der Datenpunkte und Länge einer Route nach der Simplifizierung durch den Ramer-Douglas-Peucker-Algorithmus.}
	\label{fig:grundlagen-vereinfachung-epsilon}
\end{table}

Wie oben erläutert, verringert der gegebene Algorithmus die Anzahl der Datenpunkte einer Route deutlich. Dies ermöglicht die benötigte Zeit bei weiteren durchgeführten Berechnungen zu reduzieren. Der negative Aspekt dieser Simplifizierung ist allerdings, dass auch die Datenqualität für Werte wie der Zeit, Geschwindigkeit \etc darunter leiden. Diese Daten sind bei den gegebenen Datensätzen an GPS-Koordinaten geknüpft und gehen durch deren Entfernen verloren.
Eine Lösung hierfür wäre es, die simplifizierte Route nur für den Vergleich von zwei Routen zu verwenden und die Daten der vollständigen Route beim eigentlichen Vergleich der Leistung der sporttreibenden Person heranzuziehen.
Sollte der GPS-Punkt eines Datensatzes nicht mehr Teil der Route sein, so wird dieser auf den naheliegendsten Punkt (siehe Punkt~\vref{subsec:grundlagen-formeln-naechste-koordinate-auf-segment}) auf der Route verschoben und somit seiner ursprünglichen Position angenähert.
