\section{GPS}
\label{sec:grundlagen-gps}

GPS (Global Positioning System) dient der geographischen Ortung auf der Erde. Hierzu werden insgesamt 24 Satelliten verwendet, die sich in der mittleren Erdumlaufbahn mit einem Abstand von \ca \SI{20200}{\kilo\meter} zur Erde befinden~\cites[69]{kaplan2005understanding}[44]{kaplan2005understanding}[60]{xu2003gps}. Diese umkreisen die Erde so, dass von jedem Punkt auf der Erde immer mindestens 4 Satelliten sichtbar sind.

Jeder Satellit besitzt eine Atomuhr. Die Empfänger erhalten nach~\cite[31]{allan1997science} die Uhrzeit mit einer Genauigkeit von~\SI{14}{\nano\second}, wobei ein typischer GPS-Empfänger nach~\cite{galleon2018gpst} und~\cite[1]{dana2011therole} die Zeit nur mit einer Genauigkeit von~\SI{100}{\nano\second} bis \SI{1}{\milli\second} verarbeitet. Zudem ist jedem Satellit seine aktuelle exakte Position bekannt. Diese beiden Informationen werden durch den Satelliten an die Empfangsgeräte auf der Erde geschickt. Die Empfangsgeräte wiederum kommunizieren nicht direkt mit den Satelliten, sondern verarbeiten lediglich die empfangenen Daten.

Um die tatsächliche Position zu bestimmen, benötigt der Empfänger die aktuelle Uhrzeit und Positionsinformationen von mindestens 4 Satelliten. Je mehr Informationen er erhält, desto genauer wird die Positionsbestimmung.

Die Positionsbestimmung selbst erfolgt über Trilateration. Dies bedeutet, dass zunächst über die Differenz der erhaltenen Uhrzeiten der jeweilige Abstand zu den Satelliten berechnet werden kann. Durch die Kenntnis über den Standort der entsprechenden Satelliten, können um diese jeweils eine Sphäre aufgespannt werden~(siehe Abbildung~\vref{fig:grundlagen-gps-trilateration}). Der Überschneidungspunkt dieser Sphären entspricht der aktuellen Position des Empfängers.~\cite{trilateration}

\begin{figure}
	\centering
	\includegraphics[width=0.5\linewidth]{grundlagen/gps/trilateration}
	\caption{GPS Trilateration zur Positionsbestimmung.~\cite{trilateration}}
	\label{fig:grundlagen-gps-trilateration}
\end{figure}

Der durchschnittliche Fehler (\SI{95}{\percent}) bei einer geographischen Ortung mittels GPS beträgt nach~\cite[S.~B-17]{united2001global} \SI{7.8}{\meter}.
Durch die Verwendung von
\begin{itemize}
	\item Differential Global Positioning System (siehe Punkt~\vref{subsec:grundlagen-gps-dgps}) und
	\item Assisted Global Positioning System (siehe Punkt~\vref{subsec:grundlagen-gps-agps})
\end{itemize}
kann diese Genauigkeit jedoch erhöht werden.

\subsection{Differential Global Positioning System}
\label{subsec:grundlagen-gps-dgps}

DGPS (Differential Global Positioning System) ermöglicht die Verbesserung der GPS-Lokalisierung mit Hilfe von auf der Erde stationierten Basisstationen.
Diese Referenzstationen besitzen zum einen einen GPS-Empfänger, zum anderen die Möglichkeit direkt mit anderen beweglichen GPS-Empfängern zu kommunizieren.
Bei der Einrichtung einer Basisstation wird dessen GPS-Position geodätisch exakt ermittelt.~\cite[115]{mansfeld2004satellitenortung}

Durch den Empfänger der Referenzstation wird die GPS-Position unter Verwendung der verfügbaren Satelliten ermittelt.
Das Gerät kann nun den Fehler zwischen der vorgegebenen und der gemessen GPS-Position bestimmen.
Diese Korrekturdaten werden an umliegende bewegliche GPS-Empfänger gesendet, die die Genauigkeit der GPS-Ortung verbessern.
Diese Endgeräte passen die selbst gemessenen GPS-Koordinaten, entsprechend der erhaltenen Abweichungen durch die Stationen, an.
Dieses Verfahren ermöglicht eine Genauigkeit zwischen unter einem Meter bis zu \SI{5}{\meter}, abhängig von der Distanz zur Basisstation.
\cite[79--80]{el2002introduction}


\subsection{Assisted Global Positioning System}
\label{subsec:grundlagen-gps-agps}

Bei der Verwendung von herkömmlichem GPS treten mehrere Probleme auf, die durch die Verwendung von AGPS (Assisted Global Positioning System) behoben \bzw abgeschwächt werden~\cite[341]{mansfeld2009satellitenortung}:
\begin{itemize}
	\item Die Dauer der ersten Positionsbestimmung ist abhängig von der Aktualität der Bahndaten der Satelliten.
	\item Es wird eine längere Zeit zur Positionsbestimmung benötigt, wenn das Gerät deaktiviert wurde oder keinen Empfang hatte.
	\item Eine Unterbrechung führt ebenfalls zu einer langwierigen Positionsbestimmung.
\end{itemize}

All diese Probleme treten potentiell bei Anwendern von Tracking-Applikationen auf, da diese
\begin{itemize}
	\item nicht durchgängig das GPS aktiviert haben,
	\item nicht lange vor dem Start einer Aktivität warten wollen und
	\item gegebenenfalls Strecken mit Signalproblemen ablaufen.
\end{itemize}

AGPS löst \bzw schwächt diese Probleme ab, indem es das Mobilfunknetz nutzt.
Durch das Einwählen in ein Mobilfunknetz lässt sich der Standort des Empfangsgeräts grob ermitteln und somit der Messbereich einschränken und die Suche beschleunigen.
Zudem werden die Bahnparameter sowie die Fehlerkorrekturwerte nicht über langsame Satellitensignale geladen, sondern direkt über das Mobilfunknetz von stationären Referenzempfängern bezogen.~\cite[341]{mansfeld2009satellitenortung}