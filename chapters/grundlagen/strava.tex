\section{Strava}
\label{sec:grundlagen-strava}

In dieser Arbeit wird aufgrund der offenen und einfach zu bedienenden API (siehe~\cite{strava-api}) Strava verwendet. Strava bietet Sportlerinnen und Sportlern die Möglichkeit ihre Sportaktivitäten aufzuzeichnen und diese entweder über die bereitgestellten Applikationen oder programmatisch abzurufen. Die aufgezeichneten Daten werden über die API als sogenannte \emph{Streams} bereitgestellt. Streams entsprechen den zuvor erläuterten Datenreihen.

\subsection{Auflösung}
\label{subsec:grundlagen-strava-aufloesung}

Jeder Stream kann mit den unterschiedlichen Auflösungen
\begin{itemize}
	\item Low (Niedrig),
	\item Medium (Mittel) und
	\item High (Hoch)
\end{itemize}
abgefragt werden, auch wenn dieser Parameter nicht offiziell in~\cite{strava-api} dokumentiert ist. Diese Auflösung bezieht sich auf die Anzahl der Datenpunkte, die durch die API retourniert werden. Bei den Auflösungen \emph{Low} und \emph{Medium} werden Datenpunkte aus den Originaldaten entfernt. Die Auflösung der Daten ist für die im weiteren Verlauf dieser Arbeit durchgeführten Berechnungen relevant, da eine höhere Auflösung zwar ein genaueres Ergebnis liefert, allerdings wird zur Berechnung dieses Ergebnisses mehr Zeit benötigt.

Die Auflösung \emph{High} enthält alle verfügbaren Datenpunkte und bietet daher die höchste Qualität an Daten. \emph{Medium} retourniert immer maximal \num{1000}~Datenpunkte pro Aktivität. \emph{Low} retourniert maximal \num{100}~Datenpunkte. Dies bedeutet, dass bei den zwei niedrigen Auflösungen der Abstand zwischen den Datenpunkten von der Strecke \bzw Zeit der Aktivität abhängt. Sollte die Gesamtsumme der der Datenpunkte unter \num{1000} \bzw \num{100} liegen, so liefern \emph{Medium} und \emph{Low} entsprechend niedrigere Werte.

Die Analyse von \num{180} Datenreihen über \ca \SI{2050}{\kilo\meter} und \SI{125}{\hour} mit \ca \num{280000} aufgezeichneten Datenpunkten\footnote{\url{https://www.strava.com/athletes/15757856}} ergab, dass Strava unabhängig von der Geschwindigkeit der sporttreibenden Person alle \SIrange{1}{2}{\second} einen Datenpunkt aufzeichnet. Dies bedeutet, dass die Distanz zwischen zwei Datenpunkten bei höherer Geschwindigkeit signifikant größer wird. Demnach ist es nicht möglich in der Implementierung einen gewissen oder einen regelmäßigen Abstand zwischen den Datenpunkten anzunehmen.

Die Strava-API bietet pro Aktivität drei Datenreihen mit festgelegten Präzisionen an. Um für die Berechnungen in dieser Arbeit mehr Flexibilität im Umfang der Datenreihen zu haben, wird in Abschnitt~\vref{sec:grundlagen-vereinfachung} ein Verfahren erläutert, um aus den Originaldaten verschiedene Datenreihen mit beliebigen Präzisionen zu berechnen.

Ebenfalls undokumentiert ist die Möglichkeit bei einer Abfrage zwischen zwei Datensätzen zu wählen, die	sich in der Basis-Datenreihe bei der Vereinfachung unterscheiden:
\begin{itemize}
	\item Distance (Distanz)
	\item Time (Zeit)
\end{itemize}
Bei einer gewählten Präzision von \emph{High} unterscheiden sich die retournierten Daten nicht, da dies die Originaldaten sind und noch nicht vereinfacht wurden. Bei den Präzisionen \emph{Medium} und \emph{Low} unterscheidet sich der gesamte Datensatz. Da~\cite{strava-api} keine Informationen über die Vorgehensweise bei der Reduzierung der Datenpunkte enthält, kann von diesem Parameter nicht auf triviale Weise eine bestimmte Veränderung der Datenpunkte beobachtet werden. Somit wird im weiteren Verlauf dieser Arbeit die Standardeinstellung~\emph{Distance} verwendet.

\subsection{Verfügbare Daten}
Insgesamt werden über die Strava-API \num{11}~verschiedene Streams bereitgestellt, die in Tabelle~\vref{tab:grundlagen-strava-verfuegbare-daten} detailliert erläutert werden.

\begin{table}
	\centering
	\begin{tabular}{l|l|r|l}
		Bezeichnung    & Einheit & Genauigkeit & Beschreibung \\
		\hline
		Time           & Sekunden & \SI{1}{\second} & Dauer bis zum Datenpunkt \\
		LatLng         & Grad & \num{1e-6} ° & Aktueller Ort \\
		Distance       & Meter & \SI{0.1}{\meter}  & Distanz bis zum Datenpunkt \\
		Altitude       & Meter & \SI{0.1}{\meter} & Aktuelle Höhe \\
		SmoothVelocity & Meter & \SI{1}{\meter\per\second} & Aktuelle Geschwindigkeit \\
		Heartrate      & BPM & \SI{1}{BPM} & Aktuelle Herzfrequenz \\
		Cadence        & RPM & \SI{1}{RPM} & Aktuelle Trittfrequenz (Rad) \\
		Power          & Watt & \SI{1}{\watt} & Aktuelle Leistung \\
		Temperature    & Grad Celsius & \SI{1}{\SIUnitSymbolCelsius} & Aktuelle Außentemperatur \\
		Moving         & Wahrheitswert & --- & In Bewegung oder nicht \\
		SmoothGrade    & \% einer Schwierigkeit & \SI{1}{\percent} & Schwierigkeitsgrad (Klettern) \\
	\end{tabular}

	\caption{Verfügbare Daten über die Strava-API.}
	\label{tab:grundlagen-strava-verfuegbare-daten}
\end{table}


\subsection{LatLngStream}
\label{subsec:grundlagen-strava-latlngstream}

Die Genauigkeit in Metern einer Breitenangabe ist unabhängig des Längengrads konstant, wohingegen die Genauigkeit einer Längenangabe je nach Breitengrad variiert. Grund hierfür ist, dass sich der Abstand zwischen den Meridianen verkleinert, je weiter die entsprechende Koordinate vom Äquator entfernt ist. Aufgrund der Kugeleigenschaft der Erde ergibt sich damit, dass die Längenangaben mindestens die selbe Genauigkeit, wie die Breitenangaben besitzen. Für die in dieser Arbeit durchgeführten Berechnungen ist ausschließlich die schlechteste verfügbare Genauigkeit relevant. Daher wird dieser Fakt ignoriert und die Genauigkeit des Breitengrads für die weiteren Berechnungen herangezogen.~\cite[6--7]{duvander2010map}

Die durch die Strava-API bereitgestellten Koordinaten sind bis auf 6 Nachkommastellen genau aufgezeichnet. Dies bedeutet, dass nach~\cite{osm-genauigkeit-koordinaten} die Genauigkeit mindestens \SI{111.32}{\milli\meter} beträgt. Dies widerspricht jedoch der in Abschnitt~\vref{sec:grundlagen-gps} erläuterten maximal möglichen Genauigkeit von GPS, die ein vielfaches hiervon beträgt.