\subsection{Distanz zwischen zwei Koordinaten}
\label{subsec:grundlagen-formeln-koordinaten}

Zur Implementierung der Algorithmen (siehe Kapitel~\vref{ch:implementierung}) ist es unter anderem notwendig die Distanz zwischen zwei GPS-Koordinaten zu wissen. Dies wird benötigt, um zu ermitteln, welche Koordinaten sich nahe liegen und somit gegebenenfalls Teil eines übereinstimmenden Routenabschnitts sind.

\subsubsection{Im zweidimensionalen Raum}
\label{subsubsec:grundlagen-formeln-distanz-koordinaten-2d}

In einem zweidimensionalen Koordinatensystem mit den Punkten $A$ und $B$ kann der Satz des Pythagoras $ a^2 + b^2 = c^2 $ angewendet werden. $a$ gibt hierbei den Abstand auf der x-Achse $a = |x_B - x_A|$ und $b$ den Abstand auf der y-Achse $b = | y_B - y_A |$ an. Somit gilt Gleichung~\vref{eq:grundlagen-formlen-distanz-2d} für den Abstand der beiden Punkte.~\cite[34]{gericke2013mathematik}

\begin{equation}
\label{eq:grundlagen-formlen-distanz-2d}
	c = \sqrt{\left(x_B - x_A\right)^2 + \left(y_B - y_A\right)^2}
\end{equation}

\begin{figure}
	\centering
	\resizebox{0.7\textwidth}{!}{
		\includestandalone{images/grundlagen/formeln/distanz-koordinaten-2d}
	}

	\label{fig:grundlagen-formeln-distanz-koordinaten-2d}
	\caption{Berechnung der Distanz zwischen zwei Punkten in einem 2D-Raum.}
\end{figure}

\subsubsection{Mit GPS-Koordinaten}

Die im zweidimensionalen Raum berechnete Distanz zwischen zwei Punkten lässt sich nicht direkt auf Polarkoordinaten anwenden. Entweder können hierzu die gegebenen Koordinaten umgewandelt werden oder es müssen zusätzliche Umwege gegangen werden, die nachfolgend erläutert werden.

Auf einer Kugeloberfläche beschreiben Othodrome die kürzeste Verbindung zwischen zwei Punkten. Ein Othodrom ist immer ein Teilstück eines Großkreises, der um die gesamte Kugel verläuft und die Erde als Zentrum hat.~\cite[111]{schertenleib2008grundlagen}

Zur Berechnung der Länge eines Othodroms muss zunächst der Winkel zwischen den Punkten sowie dem Mittelpunkt der Kugel ermittelt werden. Hierzu wird die Haversine benötigt. Diese ist die Hälfte der in Abbildung~\vref{fig:grundlagen-formeln-versine} eingezeichneten Versine. Diese lässt sich demnach wie in Gleichung~\vref{eq:grundlagen-formeln-haversine} dargestellt berechnen.~\cite[78]{abramowitz1964handbook}

\begin{equation}
    \label{eq:grundlagen-formeln-haversine}
    \text{hav} \left( \theta \right) = \sin^2 \left( \frac{\theta}{2} \right) = \frac{1 - \cos \left( \theta \right)}{2}
\end{equation}

\begin{figure}
	\centering
	\resizebox{0.7\textwidth}{!}{
		\includestandalone{images/grundlagen/formeln/versine}
	}

	\caption{Sinus, Kosinus und Versine zwischen zwei Punkten auf einem Einheitskreis.}
	\label{fig:grundlagen-formeln-versine}
\end{figure}

Für ein Othodrom der Länge $d$ auf einer Kugel mit dem Radius $r$ gilt Gleichung~\vref{eq:grundlagen-formeln-distanz-ursprung}. $ \frac{d}{r} $~beschreibt hierbei den zuvor erläuterten Winkel zwischen den zwei Punkten und dem Mittelpunkt der Kugel.

\begin{equation}
    \label{eq:grundlagen-formeln-distanz-ursprung}
	\text{hav} \left( \frac{d}{r} \right) = \text{hav} \left( \phi_2 - \phi_1 \right) + \cos \left( \phi_1 \right) \text{hav} \left( \lambda_2 - \lambda_1 \right)
\end{equation}

Nach dem Umstellen der Gleichung nach $d$ ergibt sich für die Länge des Othodroms die Gleichung~\vref{eq:grundlagen-formeln-distanz-punkte}~\cite{chamberlai1996greatcircle}. Diese Gleichung lässt sich auf Kugeln mit verschiedenen Radien~$r$ anwenden. Für den gegebenen Anwendungsfall entspricht der Radius der Erde am Äquator \SI{6378}{\kilo\meter}~\cite{torge2001geodesy}.

\begin{equation}
    \label{eq:grundlagen-formeln-distanz-punkte}
	d = 2r \arcsin \left(
		\sqrt{\sin^2 \left( \frac{\phi_2 - \phi_1}{2} \right) + 
		\cos \left( \phi_1 \right) \cos \left( \phi_2 \right) \sin^2 \left( \frac{\lambda_2 - \lambda_1}{2} \right) }
	\right)
\end{equation}

Diese Formel ermöglicht die Berechnung einer genauen Länge eines Othodroms auf einer Kugel. Dies führt allerdings bei dem gegebenen Anwendungsfall zu einem Problem, da die Erde nicht überall den selben Radius hat und daher Höhenunterschiede zwischen den gegebenen Punkten nicht einberechnet werden können~\cite{chamberlai1996greatcircle}.
Nach~\cite[10]{1997admiralty} beträgt die Ungenauigkeit maximal~\SI{0.5}{\percent}. Diese Ungenauigkeit ist für den Anwendungsfall und die in dieser Arbeit gemessenen Distanzen vernachlässigbar.