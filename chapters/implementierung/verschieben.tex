\section{Verschieben von Routen auf reale Straßen}
\label{sec:implementierung-verschieben}

Ein Problem bei der Analyse der über die Strava-API (siehe Abschnitt~\vref{sec:grundlagen-strava}) bereitgestellten Daten ist deren Genauigkeit. Wie in Abschnitt~\vref{sec:grundlagen-gps} sowie unter Punkt~\vref{subsec:grundlagen-strava-latlngstream} beschrieben, kann die ermittelte Position um mehrere Meter von der tatsächlichen Position während der Aufzeichnung abweichen.

Daher bieten Google Maps sowie OpenStreetMap die Möglichkeit an, Koordinaten von übermittelten Routen auf nahegelegene Wege und Straßen zu verschieben. Diese Funktionen retournieren, neben den bereits existierenden Punkten, gegebenenfalls neue Punkte hinzu oder entfernen Punkte aus den übermittelten Koordinaten.

Nachfolgend werden beide Schnittstellen mit Daten der Strava-API getestet und die Ergebnisse miteinander verglichen.

\subsection{Google Maps}
Der Dienst von Google versucht die ihm übergebenen Punkte auf die in der Datenbank verfügbaren Straßen zu verschieben. Sollte hierbei keine passende Straße für einen Punkt gefunden werden, so wird dieser aus der Datenreihe entfernt.~\cite{google-maps-snap-to-roads}

Die Schnittstelle ist limitiert auf maximal 100 Koordinaten pro Anfrage. Zudem sind maximal 2500 Abfragen pro Tag kostenfrei. Zusätzliche Abfragen sind kostenpflichtig.~\cite{google-maps-pricing}

Eine weitere Limitierung, die erst während der Anwendung auftaucht, ist der Maximalabstand zwischen zwei Punkten von \SI{300}{\meter}. Dies bedeutet, dass nicht beliebig weit vereinfachte Datenreihen problemlos an den Dienst übergeben werden können.

Zusätzlich bietet Google die Möglichkeit die verschobenen Punkte, wie in Abbildung~\vref{fig:implementierung-verschieben-google-maps-interpolate} dargestellt, mit der Option \emph{interpolate} zu interpolieren und somit zusätzliche Punkte hinzuzufügen~\cite{google-maps-snap-to-roads}.
Auch wenn das Interpolieren eine deutlich rundere Route ergibt, die mehr dem tatsächlichen Straßenverlauf entspricht, muss beachtet werden, dass hierdurch die Anzahl der Datenpunkte der Originalroute unverändert bleibt und somit beispielsweise Durchschnittsgeschwindigkeiten auf den neu entstandenen Segmenten nur berechnet und nicht gemessen werden können.

\begin{figure}
	\begin{subfigure}{.5\textwidth}
		\includegraphics[width=\linewidth]{implementierung/verschieben/google-maps-no-interpolate}
		\subcaption{Verschieben der existierenden Punkte}
		\label{fig:implementierung-verschieben-google-maps-no-interpolate}
	\end{subfigure}
	\begin{subfigure}{.5\textwidth}
		\includegraphics[width=\linewidth]{implementierung/verschieben/google-maps-interpolate}
		\subcaption{Verschieben der existierenden Punkte und zusätzliche interpolierte Punkte.}
		\label{fig:implementierung-verschieben-google-maps-interpolate}
	\end{subfigure}

	\caption{Verschieben der Punkte auf naheliegende Straßen durch Google Maps mit und ohne Interpolation}
	\label{fig:implementierung-verschieben-google-maps}
\end{figure}

Bei der Verwendung der API gilt zu beachten, dass ausschließlich Straßen für Autos als potentielle Routen verwendet werden. Auf Rad- oder Fußwege werden die übergebenen Punkte nicht verschoben. Dies bedeutet eine große Einschränkung für den gegebenen Anwendungsfall, da hiermit die Anzahl der Aktivitäten, auf die der Dienst angewendet werden kann, stark reduziert wird.

\subsection{OpenStreetMap}
\label{subsec:implementierung-verschieben-osm}
OpenStreetMap selbst bietet keine solche API an. Mapbox (siehe~\cite{mapbox}) hingegen stellt die Möglichkeit auf Basis der Daten von OpenStreetMap zur Verfügung, eine Route an existierende Straßen und Wege anzupassen~\cite{mapbox-map-matching}.

Diese Schnittstelle ist limitiert auf
\begin{itemize}
	\item 60 Anfragen pro Minute sowie
	\item maximal 100 Koordinaten pro Anfrage.
\end{itemize}

Die API bietet mehrere Parameter an, über die das Ergebnis an die eigenen Bedürfnisse angepasst werden kann. Für den gegebenen Anwendungsfall ist nur der Parameter \emph{overview} relevant. Alle weiteren Parameter sind in der Dokumentation erläutert~\cite{mapbox-api}. Dieser Parameter gibt an, ob das Ergebnis mit maximaler Qualität zurückgegeben werden soll und zusätzliche Punkte wie bei der Verwendung von Google Maps interpoliert werden sollen.

Mapbox ermöglicht es \num{6000} Punkte pro Minute auf nahegelegene Straßen und Wege zu verschieben. Bei einer Radstrecke über fast \SI{2}{\hour}, mit der Auflösung~\emph{High} und \num{6662} Punkten, würde dies eine Mindestwartezeit von \SI{1}{\min} bedeuten. Da eine entsprechende Radaktivität auch eine entsprechend lange Zeit auf dem Sattel erfordert, sollte es in einer produktiven Applikation möglich sein, dem Benutzer eine kurze Zeit warten zu lassen. Alle Aktivitäten mit unter \num{6000} Datenpunkten können ohne Beschränkung an die API übergeben werden und stellen somit kein Problem dar.

Für die Anwendung im Echtzeitbereich ist die Mapbox API allerdings gut geeignet, da hier nur alle paar Sekunden ein neuer Datenpunkt hinzugefügt wird und somit die Limitierungen keine Beschränkung darstellen.
Hier muss allerdings beachtet werden, dass einige Datenpunkte als Kontext mitgegeben werden müssen, damit Mapbox ein genaues Ergebnis berechnen kann.
Auch bei einer Berechnung im Anschluss an eine Aktivität kann die Möglichkeit benutzt werden, bereits während der Aktivität alle \num{100} Datenpunkte eine Anfrage an den Server zu schicken.

Mapbox bietet die Möglichkeit die erkannten Straßen, entsprechend des verwendeten Fahrzeugs, zu filtern. Hierbei stehen Daten für
\begin{itemize}
	\item Autofahrten,
	\item Fußgänger und
	\item Radfahrer
\end{itemize}
zur Verfügung.

\subsection{Fazit}
Das wichtigste Kriterium zur Wahl zwischen Google Maps und Mapbox ist, welcher der beiden Dienste zur Darstellung der Ergebnisse verwendet wird. Beide Dienste nutzen zur Berechnung der verbesserten Routenverläufe jeweils ihre eigenen Kartendaten, die jedoch voneinander abweichen. Zur Berechnung von den gegebenen Problemstellungen ist dies zwar kein Problem, doch bei der Darstellung treten teilweise Ungenauigkeiten, wie in Abbildung~\vref{fig:implementierung-verschieben-google-maps-ungenau} sichtbar, auf.

\begin{figure}
	\includegraphics[width=\linewidth]{implementierung/verschieben/google-maps-ungenau}
	\caption{Ungenauigkeiten bei der Darstellung der Ergebnisse von Google Maps durch Mapbox.}
	\label{fig:implementierung-verschieben-google-maps-ungenau}
\end{figure}

Da Strava auch für Sportaktivitäten auf unbefestigten Straßen verwendet werden kann, darf den Ergebnissen der Dienste nicht blind vertraut werden. Wie in Abbildung~\vref{fig:implementierung-verschieben-strand} sichtbar führt beispielsweise eine Aktivität am Strand, bei der Verwendung von Google Maps, zu falschen Ergebnissen. Da die Mapbox API im Gegensatz dazu einen Parameter \emph{radiuses} hat, der die maximal zu verschiebende Distanz angibt, wird bei der selben Route ein Fehler retourniert.

\begin{figure}
	\includegraphics[width=\linewidth]{implementierung/verschieben/strand}
	\caption{Fehlerhaftes Ergebnis durch Google Maps bei einer Laufaktivität am Strand.}
	\label{fig:implementierung-verschieben-strand}
\end{figure}

Ein signifikanter Nachteil der Google Maps API ist die Verwendung von Karten für Kraftfahrzeuge. Dies bedeutet, dass viele Wege, die durch die Sportlerin oder den Sportler verwendet werden, nicht erkannt werden. Somit liefert die Schnittstelle ein falsches Ergebnis.

Abschließend lässt sich sagen, dass beide APIs ihre Vor- und Nachteile mit sich bringen. Aufgrund der Einschränkung der verfügbaren Wege, wird im weiteren Verlauf der Arbeit die Schnittstelle von Mapbox, der von Google Maps, vorgezogen.

Die Problematik der begrenzten Anzahl an Datenpunkten pro Abfrage stellt für den reinen Vergleich der Routen kein Problem dar, da die Strava-API für jede Aktivität einen Datensatz mit genau \num{100} Datenpunkten bereitstellt (siehe Abschnitt~\vref{sec:grundlagen-strava}). Zum Vergleich der Leistung der sporttreibenden Person ist dieser Ansatz allerdings nicht geeignet, da dies eine enorme Reduktion der Datenqualität bedeutet.
Alternativ hierzu besteht beispielsweise die Möglichkeit den Datensatz mit \num{1000} oder mit der maximalen verfügbaren Anzahl an Punkten zu verwenden und die Punkte in mehreren Abfragen auf entsprechende Straßen zu verschieben.
