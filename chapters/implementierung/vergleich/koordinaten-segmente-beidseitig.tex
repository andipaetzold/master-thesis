\subsection{Beidseitiger Vergleich der Koordinaten mit mehreren Segmenten}
\label{subsec:implementierung-vergleich-koordinaten-segmente-beidseitig}

Dieser Punkt beschreibt abermals eine Erweiterung der Implementierung des vorherigen Punktes. Hierbei wird nicht mehr zwischen Route~1 und Route~2 unterschieden, wie es zuvor geschehen ist, sondern die Koordinaten beider Routen werden mit den Segmenten der jeweils anderen Route verglichen. Dies ermöglicht erstmals, dass der Algorithmus einen unbegrenzt langen übereinstimmenden Routenabschnitt retourniert. Zuvor war dieser aufgrund der Herangehensweise auf die Länge von ein \bzw zwei Segmenten von Route~2 begrenzt.

In diesem Ansatz wechseln die Rollen der beiden Routen in Abhängigkeit des aktuellen Status des Algorithmus. Zu jedem Zeitpunkt innerhalb des Algorithmus gibt es jeweils vier relevante Datenpunkte von denen das weitere Vorgehen der Implementierung abhängt. Angenommen wir haben Route~1 mit den Punkten $a_1$, $a_2$ und $a_3$ sowie Route~2 mit den Punkten $b_1$, $b_2$. Befindet sich hierbei $b_1$ in der Nähe des Segments~$\overrightarrow{a_1 a_2}$, so werden die Punkte entsprechend Abbildung~\vref{fig:implementierung-vergleich-koordinaten-segmente-beidseitig-bezeichnung} benannt.

\begin{figure}
	\centering
	\resizebox{\textwidth}{!}{
		\includestandalone{images/implementierung/vergleich/koordinaten-segmente-beidseitig-bezeichnungen}
	}
	
	\caption{Bezeichnungen der Koordinaten beim beidseitigen Vergleich der Koordinaten mit mehreren Segmenten.}
	\label{fig:implementierung-vergleich-koordinaten-segmente-beidseitig-bezeichnung}
\end{figure}

Im Verlauf des Algorithmus versucht der Algorithmus sich Stück für Stück über beide Routen zu arbeiten.
Hierbei wird jeweils abgewägt zum übereinstimmenden Routenabschnitt welcher Route die Distanz bis zum Ende des aktuellen Segments hinzugefügt werden soll. Dies bedeutet, dass die im vorherigen Abschnitt beschriebene Vorgehensweise in diesem Ansatz nicht alternierend durchgeführt wird sondern potentiell mehrere Segmente zum übereinstimmenden Routenabschnitt von Route~A hinzugefügt werden. Zu Route~B werden derweil nur die Segmentabschnitte bis zum nächsten Punkt auf dem aktuellen Segment zur Endkoordinate des neuen Segments von Route~A hinzugefügt.

Zunächst werden hierzu folgende zwei Abstände berechnet:
\begin{itemize}
	\item $ d_1 = \operatorname{distanceToLine}(b_\text{front}, [ a_\text{back}, a_\text{front} ]) $
	\item $ d_2 = \operatorname{distanceToLine}(a_\text{front}, [ b_\text{back}, b_\text{front} ]) $
\end{itemize}
Gilt $d_1 <= d_2$, so wird der Routenabschnitt bis zu $b_\text{front}$ sowie bis zur nächsten Koordinate von $b_\text{front}$ auf $\overrightarrow{a_\text{back}a_\text{front}}$ hinzugefügt.
Gilt $d_1 > d_2$, so wird der Routenabschnitt bis zu $a_\text{front}$ sowie bis zur nächsten Koordinate von $b_\text{front}$ auf $\overrightarrow{b_\text{back}b_\text{front}}$ hinzugefügt.

Um den Algorithmus in einen Zustand zu bringen, der es ermöglicht die Datenpunkte entsprechend Abbildung~\vref{fig:implementierung-vergleich-koordinaten-segmente-beidseitig-bezeichnung} zu beschriften wird über beide Routen iteriert und Teile des im vorherigen Punkt erläuterten Algorithmus angewendet.

Eine gekürzte Implementierung ist in Listing~\vref{lst:implementierung-vergleich-koordinaten-segmente-beidseitig} abgebildet. Auch diese hat, wie bereits alle vorherigen Implementierungen, eine Laufzeitkomplexität von $O(m n)$, wobei $m$ der Anzahl der Datenpunkte von Route~A und $n$ der Anzahl der Datenpunkte von Route~B entspricht.

\begin{lstlisting}[float,
	language={JavaScript},
	caption={Gekürzte Implementierung des beidseitigen Vergleichs der Koordinaten mit mehreren Segmenten.},
	label={lst:implementierung-vergleich-koordinaten-segmente-beidseitig}]
const overlappingPaths = [];

for (let index1 = 0; index1 < path1.length - 1; ++index1) {
  const back1 = () => path1[index1];
  const front1 = () => path1[index1 + 1];
  const line1 = () => [ back1(), front1() ];

  for (let index2 = 0; index1 < path1.length - 1 index2 < path2.length - 1; ++index2) {
    const back2 = () => path2[index2];
    const front2 = () => path2[index2 + 1];
    const line2 = () => [  path2[index2], front2() ];

    let distanceToLine1 = distance(back2(), line1());
    let distanceToLine2 = distance(back1(), line2());

    if (Math.min(distanceToLine1, distanceToLine2) > maxDistance)
      continue;

    const overlappingPath = {
      route1: [/* ... */],
      route2: [/* ... */],
    };

    while (index1 < path1.length - 1 && index2 < path2.length - 1) {
      distanceToLine1 = distanceToLine(front2(), line1());
      distanceToLine2 = distanceToLine(front1(), line2());

      if (Math.min(distanceToLine1, distanceToLine2) > maxDistance)
        break;

      if (distanceToLine1 < distanceToLine2) {
        ++index2;
      } else {
        ++index1;
      }
      
      overlappingPath.route1.push(/* ... */);
      overlappingPath.route2.push(/* ... */);
    }

    if (overlappingPath.route1.length >= 2) {
      overlappingPaths.push(overlappingPath);
    }
  }
}
\end{lstlisting}

Das Ergebnis dieses verbesserten Algorithmus zeigt, dass eine deutlich höhere Übereinstimmung wie zuvor erreicht wird.
Selbst bei der Datenauflösung \emph{Low} wird hier bei zwei identischen Beispielrouten\footnote{\url{https://strava.com/activities/1519987263} und \url{https://strava.com/activities/1489204090}} über \SI{75}{\percent} Übereinstimmung erkannt.
Grund hierfür ist, dass die Übereinstimmung nicht mehr vom Abstand der Datenpunkte \bzw der Länge der Segmente abhängt, sondern ausschließlich von der Nähe der entsprechenden Segmente der zu vergleichenden Route.
Dies bedeutet, dass ein gutes Ergebnis bereits mit wenigen Datenpunkten erreicht werden kann. Dennoch spiegelt sich eine höhere Auflösung der Datenpunkte in einer höheren Auflösung der zu vergleichenden Datenreihen (\zB Geschwindigkeit oder Herzfrequenz) wieder.
Zusätzlich ist die Anzahl der Routenabschnitte deutlich geringer, da die Implementierung es ermöglicht mehr wie zwei Segmente in einem zusammenhängenden Routenabschnitt zu erkennen.

Mit dieser Implementierung besteht nur noch ein Ausnahmefall in dem zwei Routen näher wie \verb|maxDistance| aneinander liegen, aber keiner der Anfangs- und Endkoordinaten sich in der Nähe der jeweils anderen Segmente befinden. Dies ist nur möglich, wenn sich zwei Segmente der Routen kreuzen und die entsprechenden Segmente lang genug sind, damit die dazugehörigen Koordinaten genügend Distanz zum entsprechenden Segment haben. Konkret muss die Länge beider Segmente mindestens doppelt so lang wie \verb|maxDistance| sein, damit dieser Ausnahmefall eintreten kann. Dies bedeutet wiederum, dass dieser Zustand vermieden werden kann, wenn darauf geachtet wird, dass keine zu lange Segmente miteinander verglichen werden. Das kann beispielsweise dadurch erreicht werden, dass die Vereinfachung der Route nur bis zu einem gewissen Grad möglich ist, oder, dass zusätzliche Datenpunkte eingefügt werden, um die Länge der Segmente zu reduzieren.