\subsection{Vergleich der Koordinaten}
\label{subsec:implementierung-vergleich-koordinaten}

Aufgrund der im vorherigen Punkt erläuterten Problematik müssen die zu implementierenden Algorithmen jeweils beide Routen direkt miteinander vergleichen. Hierzu wird in diesem Punkt die, aufgrund der verfügbaren Daten offensichtlichste Variante, implementiert.
Diese Herangehensweise vergleicht ausschließlich die Koordinaten der gegebenen Routen miteinander.
Hierbei wird jeweils zwischen den Anfangs- und Endkoordinaten der Segmente beider Routen die Distanz berechnet. Unterschreiten beide Abstände einen bestimmten Wert, so werden die eingeschlossenen Segmente als überlappend retourniert.

Dieser maximale Abstand lässt sich als Parameter \verb|maxDistance| konfigurieren. Der Parameter muss, abhängig von der Genauigkeit der GPS-Aufzeichnung sowie des erwarteten Fehlers, gewählt werden. Beispielsweise ist die Ungenauigkeit in einer Stadt mit hohen Häusern größer, was wiederum auch einen höheren Wert für \verb|maxDistance| bedeutet. Zudem sollte darauf geachtet werden, dass der Parameter nicht zu klein gewählt wird, da ansonsten bereits das Laufen auf der anderen Straßenseite nicht als Übereinstimmung erkannt wird.

Die Referenzwerte dieses Algorithmus sind jeweils die Anfangs- und Endkoordinaten zwischen denen der Abstand berechnet wird.

Daraus folgt die in Listing~\vref{lst:implementierung-vergleich-koordinaten} dargestellte Implementierung, die zusätzlich in Abbildung~\vref{fig:implementierung-vergleich-koordinaten} graphisch dargestellt ist.

\begin{figure}
	\centering
	\resizebox{\textwidth}{!}{
		\includestandalone{images/implementierung/vergleich/koordinaten}
	}
	
	\caption{Vergleich von zwei Routen über deren Koordinaten.}
	\label{fig:implementierung-vergleich-koordinaten}
\end{figure}

\begin{lstlisting}[float, language={JavaScript}, caption={Vergleich der Punkte von zwei Routen}, label={lst:implementierung-vergleich-koordinaten}]
function compare(path1, path2, maxDistance) {
  const result = [];

  for (let indexPath1 = 0; indexPath1 < path1.length - 1; ++indexPath1) {
    for (let indexPath2 = 0; indexPath2 < path2.length - 1; ++indexPath2) {
      const d1 = distance(path1[indexPath1], path2[indexPath2]);
      const d2 = distance(path1[indexPath1 + 1], path2[indexPath2 + 1]);
      if (d1 <= maxDistance && d2 <= maxDistance) {
        result.push(/* ... */);
        break;
      }
    }
  }

  return result;
}
\end{lstlisting}

Aufgrund der verschachtelten Schleife ergibt sich für diesen Schritt des Algorithmus eine Zeitkomplexität von $O(m n)$, bei der $m$ sowie $n$ der Gesamtanzahl der Datenpunkte in einer der beiden Routen entspricht. Aufgrund der Unabhängigkeit der einzelnen Iterationen lässt sich diese Schleife leicht parallelisieren, was zu einer deutlichen Reduktion der Berechnungszeit in einer produktiven Version führen würde.

Das Problem dieser Implementierung wird bei der Verwendung des Prototypen deutlich. Selbst bei zwei identischen Routen über ca. \SI{11}{\kilo\meter}\footnote{\url{https://strava.com/activities/1519987263} und \url{https://strava.com/activities/1489204090}} wird bei einer hohen Routenauflösung kaum eine Übereinstimmung zwischen den Routen ermittelt.
Wird die Route jedoch, wie in Abschnitt~\vref{sec:grundlagen-vereinfachung} erläutert, weiter und weiter vereinfacht, so erkennt der Algorithmus immer mehr Übereinstimmungen.
Dies liegt daran, dass durch die Vereinfachung nur Punkte mit signifikanten Richtungsänderungen erhalten bleiben und diese sich auch bei zwei unterschiedlichen Sportaktivitäten an ähnlichen Orten befinden. Somit werden in beiden Aktivitäten ähnliche Punkte entfernt und die restlichen Punkte bilden Segmente, die, entsprechend der Beschreibung dieses Algorithmus, übereinstimmen.

Verschiebt man zudem die Koordinaten der gegebenen Route, mittels einer der unter Abschnitt~\vref{sec:implementierung-verschieben} erläuterten Dienste, auf einen nahegelegenen Weg oder Straße, so erhält man erneut ein deutlich besseres Ergebnis.
Dies liegt daran, dass die Route nur noch Punkte enthält an denen signifikante Richtungsänderungen stattfinden. Eben diese werden durch den genannten Algorithmus noch näher zusammengeschoben und befinden sich danach sehr wahrscheinlich an den selben Kurven oder Kreuzungen der Routen. Befindet sich zudem beispielsweise ein Punkt in der Nähe einer \ca \ang{90} Kurve oder Kreuzung, so steigt die Wahrscheinlichkeit, dass die Punkte beider Routen auf die identische GPS-Koordinate verschoben werden.