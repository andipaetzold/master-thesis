\subsection{Vergleich der Koordinaten mit mehreren Segmenten}
\label{subsec:implementierung-vergleich-koordinaten-segmente}

Im vorherigen Abschnitt wurde erläutert, wie die Anfangs- und Endkoordinaten von Route~A mit den Segmenten von Route~B verglichen werden. Dies verbessert das Ergebnis deutlich, weist allerdings auch Schwächen der Vorgehensweise auf. Ein Nachteil ist, dass hier ausschließlich Routenabschnitte übereinstimmen, bei denen das Segment von Route~A kleiner oder gleich lang wie das Segment von Route~B ist. Zudem muss sich das gesamte Segment von Route~A zwischen den Anfangs- und Endkoordinaten des Segments von Route~B befinden. Dies bedeutet, dass selbst bei einer identischen Route potentiell keine Überschneidung gefunden werden würde, wenn die Koordinaten der beiden Routen auf der selben Straße alternieren.

Um dieses Problem zu beheben, werden die Start- und Endkoordinaten der Segmente von Route~1 weiterhin mit den Segmenten von Route~2 verglichen. Dieses Mal besteht allerdings die Möglichkeit, dass das Segment, das sich nah an der Anfangskoordinate befindet, nicht mit dem Segment übereinstimmen muss, das sich nah an der Endkoordinate befindet. Es wird lediglich vorausgesetzt, dass diese Segmente hintereinander auf der Route vorkommen und dass die Anfangskoordinate auch in der Nähe des früheren Segments von Route~2 liegt. In Abbildung~\vref{fig:implementierung-vergleich-koordinaten-segmente} wird dargestellt, wie sich hierbei die Anzahl der überlappenden Routenabschnitte erhöht.

\begin{figure}
	\centering
	\resizebox{\textwidth}{!}{
		\includestandalone{images/implementierung/vergleich/koordinaten-segmente}
	}
	
	\caption{Vergleich der Koordinaten von Route~1 mit den Segmenten von Route~2.}
	\label{fig:implementierung-vergleich-koordinaten-segmente}
\end{figure}

Eine beispielhafte Implementierung dieses Ansatzes ist in Listing~\vref{lst:implementierung-vergleich-koordinaten-segmente}~ dargestellt.
Hierbei wird jedes Segment von Route~1 mit allen Segmentenpaaren von Route~2 verglichen. Segmentenpaare sind zwei aufeinanderfolgende Segmente. Die Abstände der Endkoordinaten des Segmente von Route~1 zu beiden Segmenten des Segmentenpaars von Route~2 werden berechnet und jeweils mit \verb|maxDistance| verglichen. Falls mindestens einer der Abstände kleiner oder gleich wie \verb|maxDistance| ist, so wird der Routenabschnitt, mit dem entsprechenden nächsten Segment des Segmentenpaars, den überlappenden Abschnitten hinzugefügt.
Durch die zwei verschachtelten Schleifen ergibt sich eine Laufzeitkomplexität Implementierung von $O(m n)$. Hierbei entspricht $m$ der Anzahl der Datenpunkte von Route~1 und $n$ der Anzahl der Datenpunkte von Route~2 entspricht.

\begin{lstlisting}[float, language={JavaScript}, caption={Vergleich der Koordinaten einer Route mit den Segmenten einer anderen Route.}, label={lst:implementierung-vergleich-koordinaten-segmente}]
function compare(path1, path2, maxDistance) {
  const result = [];

  for (let indexPath1 = 0; indexPath1 < path1.length - 1; ++indexPath1) {
    const startPath1 = pathCoords1[indexPath1];
    const stopPath1 = pathCoords1[indexPath1 + 1];
  
    for (let indexPath2 = 0; indexPath2 < path2.length - 2; ++indexPath2) {
      const line1 = [ path2[indexPath2], path2[indexPath2 + 1] ];
      const line2 = [ path2[indexPath2 + 1], path2[indexPath2 + 2] ];
  
      const startPath2 = nearestPointOnLine(startPath1, line1);
      const distanceToStart = distance(startPath1, startPath2);
  
      const stopPath2_1 = nearestPointOnLine(stopPath1, line1);
      const stopPath2_2 = nearestPointOnLine(stopPath1, line2);
  
      const distanceToStop1 = distance(stopPath2_1, stopPath1);
      const distanceToStop2 = distance(stopPath2_2, stopPath1);
  
      const stopPath2 = 
        distanceToStop1 < distanceToStop2 ? stopPath2_1 : stopPath2_2;
      const distanceToStop = Math.min(distanceToStop1, distanceToStop2);
  
      if (distanceToStart <= maxDistance && distanceToStop <= maxDistance) {
        overlappingPaths.push(/* ... */);
        break;
      }
    }
  }

  return result;
}
\end{lstlisting}

Wie in Abbildung~\vref{fig:implementierung-vergleich-koordinaten-segmente-problem} dargestellt, kann dieser Ansatz allerdings auch zu sehr falschen Ergebnissen führen. Dies kann auftreten, wenn zwischen den zwei Segmenten von Route~2 eine signifikante Richtungsänderung stattfindet und die Punkte des Segments von Route~1 weit vom Übergang zwischen den Segmenten von Route~2 entfernt ist.
Dies kann behoben werden, indem ein maximaler Abstand zwischen der Koordinate zwischen dem Segmentenpaar und dem Segment von Route~1 festgelegt wird.
Dieser Datenpunkt wird zusätzlich als ein weiterer Referenzpunkt retourniert, wodurch die Qualität der Daten für deren Auswertung erhöht wird. Somit retourniert der Algorithmus Routenabschnitte mit je drei Referenzpunkten.

\begin{figure}
	\centering
	\resizebox{0.9\textwidth}{!}{
		\includestandalone{images/implementierung/vergleich/koordinaten-segmente-problem}
	}
	
	\caption{Problem beim Vergleichen der Anfangs- und Endkoordinaten von Route~A mit Segmenten von Route~B.}
	\label{fig:implementierung-vergleich-koordinaten-segmente-problem}
\end{figure}

Eine Ausweitung des in diesem Abschnitt erläuterten Algorithmus auf den Vergleich der Anfangs- und Endkoordinaten von Route~1 mit mehr wie zwei Segmenten von Route~2 würde zwar eine höhere Übereinstimmung zwischen den Routen erzeugen, jedoch auch signifikante Fehler produzieren.