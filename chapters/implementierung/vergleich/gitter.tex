\subsection{Vergleich durch Gitter}
\label{subsec:implementierung-vergleich-gitter}

In diesem ersten Ansatz zur Lösung der gegebenen Problemstellung wird ein Gitter über die gesamte Karte gelegt~(siehe Abbildung~\vref{fig:implementierung-vergleich-gitter}). Der Abstand zwischen den horizontalen sowie vertikalen Linien dieses Gitters werden mit dem Parameter \verb|gridSpacing| festgelegt. Die durch die Linien entstandenen eingeschlossenen Bereiche werden als Zellen bezeichnet.

\begin{figure}
	\centering
	\includegraphics[width=0.8\linewidth]{implementierung/vergleich/gitter.png}
	
	\caption{Ein Gitter über eine Route. Die markierten Zellen wurden von der Route besucht.}
	\label{fig:implementierung-vergleich-gitter}
\end{figure}

Zur Simplifizierung des Ansatzes werden die Längen- bzw. Breitengrade als Linien herangezogen. Dies bedeutet, dass die Zellen nicht zu~\SI{100}{\percent} die gleiche Fläche aufweisen. Da sich die Zellenflächen allerdings nur minimal unterscheiden und somit kaum Einfluss auf die Ergebnisqualität haben, wird dies ignoriert.

Verlaufen Routenabschnitte von Route~A innerhalb einer Zelle, so wird diese Zelle als \emph{von Route~A besucht} markiert. Verlaufen Routenabschnitte von Route~B ebenfalls in dieser Zelle, so wird die Zelle als \emph{von Route~B besucht} markiert und beide sich in der Zelle befindlichen Routenabschnitte gelten als \emph{übereinstimmend}.

Der Parameter~\verb|gridSpacing| muss für diesen Algorithmus entsprechend der Auflösung der Routen angepasst werden. Je höher die Auflösung desto geringer muss der Wert für \verb|gridSpacing| festgelegt werden. Dies verhindert, dass die Route durch ihre hohe Auflösung mehrere signifikante Richtungsänderungen durchführt, ohne dass sie ihre Zelle verlässt.

Eine beispielhafte Implementierung dieses Algorithmus ist in Listing~\vref{lst:implementierung-vergleich-gitter} dargestellt. Hierbei wird außer acht gelassen, dass Zellen auch als \emph{besucht} markiert werden können, wenn diese keinen Datenpunkt erhalten.
Da zur Berechnung, der von zwei Routen besuchten Zellen in einem Gitter, keine verschachtelten Schleifen benötigt werden, ergibt sich eine Laufzeitkomplexität von $O(m + n)$. Hier entspricht $n$ der Anzahl der Datenpunkte in Route~1 und $m$ der Anzahl der Datenpunkte in Route~2.

\begin{lstlisting}[float, language={JavaScript}, caption={Implementierung eines Algorithmus zum markieren von besuchten Zellen in einem Gitter.}, label={lst:implementierung-vergleich-gitter}]
function calcVisitedCells(path1, path2, gridSpacing) {
  const visitedCellsPath1 = calcVisitedCells(path1, gridSpacing);
  const visitedCellsPath2 = calcVisitedCells(path2, gridSpacing);
  
  const visitedCells = [];
  for (let i = 0; i < visitedCellsPath1.length; ++i) {
  	const cell = visitedCellsPath1[i];
    if (visitedCellsPath2.includes(cell)) {
      visitedCells.push(cell);
    }
  }
  return visitedCells;
}

function calcVisitedCellsPath(path, gridSpacing) {
  const cells = new Set();
  
  for (let i = 0; i < path.length; ++i) {
    const point = path[i];
    const cell = {
      lat: point.lat - (point.lat % gridSpacing),
      lng: point.lng - (point.lng % gridSpacing),
    };    
    cells.add(cell);
  }
  
  return [...cells];
}

\end{lstlisting}

Ein Problem dieses Ansatzes ist, dass Routenabschnitte, unabhängig von deren aktuellen Richtung, als übereinstimmend erkannt werden.
Dies bedeutet, dass bei Wegen, die in mehr wie einer Richtung absolvierbar sind, auch zwei entgegengesetzte Routenabschnitte übereinstimmend erkannt werden.
Dies hat vor allem dann eine signifikante Auswirkung auf das Ergebnis, wenn die Straße ein Gefälle besitzt. Eine Lösung für dieses Problem wäre es, die übereinstimmenden Routenabschnitte erneut zu vergleichen und hierbei eine ungefähre Übereinstimmung der Himmelsrichtung ihres Verlaufs als Bedingung zu stellen.

Ein weiteres Problem, das auftreten kann, ist, dass die Linie zwischen zwei Zellen auf einer genutzten Straße verläuft. Dies bedeutet, dass trotz Verwendung der identischen Straße die beiden Routen, abhängig von der jeweiligen Genauigkeit, zwischen den beiden Zellen wechseln oder sich in unterschiedlichen Zellen befinden. Ist \verb|rasterGap| sehr klein gewählt, so kann dies vor allem bei ungenauen Aufzeichnungen häufig vorkommen. Kein Problem hingegen ist eine Straße, die in Schlangenlinien zwischen zwei Zellen verläuft. Dies führt zwar zu vielen kleinen übereinstimmenden Routenabschnitten, die allerdings im Anschluss leicht zusammengeführt werden können.

Die Schwierigkeit, die bei dem Vergleichen von zwei Routen mit Hilfe eines Gitters entsteht, ist das Finden von Referenzpunkten auf den Routen beider Aktivitäten. Es fällt schwer eine solche Information aus dem beschriebenen Ansatz zu ermitteln, da die Datenpunkte nicht direkt miteinander verglichen werden. Es wird somit nur ein Ergebnis zurückgeliefert, das potentiell übereinstimmende Routenabschnitte beinhaltet, aber keine Verbindung zwischen diesen aufzeigt, die benötigt wird, um einen Leistungsvergleich durchzuführen.